import { ApolloServer } from "@apollo/server";
import { startServerAndCreateLambdaHandler } from '@as-integrations/aws-lambda';
import { readFileSync } from "fs";
import { resolvers } from "./graphql/resolvers.js";
import { Open5eAPI } from "./data/api/open5e.js";
import { join } from "path"

const typeDefs = readFileSync(join(__dirname, './graphql/schema.graphql'), { encoding: 'utf-8' })


// The ApolloServer constructor requires two parameters: your schema
// definition and your set of resolvers.
const server = new ApolloServer({
    typeDefs,
    resolvers,
    csrfPrevention: true
});

export const graphqlHandler = startServerAndCreateLambdaHandler(server, {
    context: async () => {
        const { cache } = server;
        return {
            dataSources: {
                open5eAPI: new Open5eAPI({ cache })
            }
        }
    }
});