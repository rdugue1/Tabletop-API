import { Class } from "../data/model/open5e/class"
import { Open5eAPI } from "../data/api/open5e.js";

export const resolvers = {
    Query: {
        classesOpen5e: async (_: any, __: any, { dataSources }: any): Promise<Class[]> =>
            dataSources.open5eAPI.getClasses(),
        classOpen5e: async (_: any, { slug }: any, { dataSources }: any): Promise<Class> =>
            dataSources.open5eAPI.getClassBySlug(slug)
    }
};

interface ContextValue {
    dataSources: {
        open5eAPI: Open5eAPI
    }
}