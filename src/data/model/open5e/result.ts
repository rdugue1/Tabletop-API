export interface Result<Type> {
    count: number,
    next: string,
    previous: string,
    results: Type[]
}