import { RESTDataSource } from '@apollo/datasource-rest';
import { Result } from "../model/open5e/result";
import { Class } from "../model/open5e/class";

export class Open5eAPI extends RESTDataSource {
    baseURL = 'https://api.open5e.com/';

    async getClasses(): Promise<Class[]> {
        try {
            return (await this.get<Result<Class>>('classes')).results;
        } catch (error) {
            console.error(error);
            return []
        }
    }

    async getClassBySlug(slug: String): Promise<Class> {
        try {
            return this.get<Class>(`classes/${slug}`);
        } catch (error) {
            console.error(error);
            throw new Error('Class not found')
        }
    }
}